<?php

include ('../lib/email/mail_helper_functions.php');
$host = $_SERVER['HTTP_HOST'];
$domain = str_replace('www.', '', str_replace('http://', '', $host));
if ($domain == 'slapdab.com') {
    define('BASE_URL', 'http://slapdab.com/');
    $con = mysqli_connect("localhost", "slapdab_bsa", "&OlUfTZIXNu7", "slapdab_land");
} else {
    define('BASE_URL', 'http://localhost/test/slabdab/');
    $con = mysqli_connect("localhost", "root", "", "slab");
}

if (!$con) {
    die('Could not connect: ' . mysqli_connect_error());
}



$return = array();
$return['error'] = 0;
$return['message'] = 0;
if (isset($_REQUEST['action']) AND $_REQUEST['action'] == 'subscribe') {
    $email = $_REQUEST['subscribe_email'];
    $email_sent = 0;
    /* Sent eamil here */
    $Subject = "Welcome to slapdab";
    // $EmailBody = file_get_contents_curl(BASE_URL . 'subscription_confirm.php?email=' . urlencode($email));

    /* mail body */
    $b = '';
    $b .='<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
            <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" name="viewport">
                <title>slabdab</title>
                <style type="text/css">
                    .ReadMsgBody { width: 100%; background-color: #ffffff; }
                    .ExternalClass { width: 100%; background-color: #ffffff; }
                    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
                    html { width: 100%; }
                    body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
                    table { border-spacing: 0;border-collapse: collapse; }
                    img { display: block !important; }
                    table td { border-collapse: collapse; }
                    .yshortcuts a { border-bottom: none !important; }
                    a { color: #e8aa9b; text-decoration: none; }
                    .textbutton a { font-family: \'open sans\', arial, sans-serif !important; color: #ffffff !important; }
                    @media only screen and (max-width: 640px) {
                        body { width: auto !important; }
                        table[class="table600"] { width: 450px !important; text-align: center !important; }
                        table[class="table-inner"] { width: 86% !important; text-align: center !important; }
                        table[class="table2-2"] { width: 47% !important; }
                        table[class="table3-3"] { width: 100% !important; text-align: center !important; }
                        table[class="table1-3"] { width: 29.9% !important; }
                        table[class="table3-1"] { width: 64% !important; text-align: center !important; }
                        table[class="footer-note"] { width: 100% !important; text-align: left !important; }
                        table[class="footer-column"] { width: 47% !important; text-align: left !important; }
                        /* Image */
                        img[class="img1"] { width: 100% !important; height: auto !important; }
                    }
                    @media only screen and (max-width: 479px) {
                        body { width: auto !important; }
                        table[class="table600"] { width: 290px !important; }
                        table[class="table-inner"] { width: 82% !important; }
                        table[class="table2-2"] { width: 100% !important; }
                        table[class="table3-3"] { width: 100% !important; text-align: center !important; }
                        table[class="table1-3"] { width: 100% !important; }
                        table[class="table3-1"] { width: 100% !important; text-align: center !important; }
                        table[class="footer-note"] { width: 100% !important; text-align: center !important; }
                        table[class="footer-column"] { width: 100% !important; text-align: center !important; }
                        /* image */
                        img[class="img1"] { width: 100% !important; }
                    }
                </style>

                </head>';
    $b .='<body>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">

                        <!--header-2-->
                        <tbody><tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody><tr>
                                                <td valign="top" bgcolor="#e8aa9b" background="http://slapdab.com/images/emailbg.jpg" height="250" style="background-size:cover;background-position:center; background-position:top;">
                                                    <!--[if gte mso 9]>
                                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
                                                            <v:fill type="tile" src="http://farm4.staticflickr.com/3754/12079806886_9a9a7cd2f7_o.jpg" color="#e8aa9b" />
                                                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                                                    <![endif]-->

                                                    <div>
                                                        <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" height="250" class="table600">
                                                            <tbody><tr>
                                                                    <td align="center" height="40"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <a href="http://www.slapdab.com/"><img width="300" alt="img" src="http://slapdab.com/images/logo/2.png"></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <tr>

                                                                </tr>
                                                                <tr>
                                                                    <td height="30"></td>
                                                                </tr>
                                                            </tbody></table>
                                                    </div>

                                                    <!--[if gte mso 9]>
                                            </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            <!--end header-2-->

                            <!-- 1/1 content -->

                            <!-- end 1/1 content -->

                            <!-- Title bar -->
                            <tr>
                                <td style="background: none repeat scroll 0% 0% rgb(247, 246, 244);">';
    $b .='<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="table600">
                                        <tbody>




                                            <tr align="left"><td style="height: 30px;"></td></tr>
                                            <tr align="left">
                                                <td style="margin-top: 0px; margin-bottom: 10px; font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; color: rgb(67, 67, 67); font-weight: 200; font-size: 18px; line-height: 27px; display: block; padding: 20px 40px; background: none repeat scroll 0% 0% rgb(255, 255, 255); border-radius: 6px;">
                                                    <div width="560px">
                                                        <p style="border-bottom: 3px dotted rgb(247, 246, 244); width: auto;"><strong style="color: rgb(0, 0, 0); font-weight: normal; font-family: Georgia,serif; margin: 0px; line-height: 40px; font-size: 30px; text-align: center ! important; display: block; padding: 20px 0px;">The Social Video Marketplace</strong></p>
                                                        <p>Thank you for signing up, since you are one of our first we shall always make sure that we remember you. </p>
                                                        <p>It is our pleasure to introduce to you a new form of marketplace. The rise of social networking has led to the emergence of social video. And we want you to stick with us, as we throw in more updates in the coming months till our full launch.</p>
                                                        <p>We request your undivided attention in helping us establish a new solution for the online marketplace.</p>
                                                        <p>Please strike us off from your junk list, if we\'ve landed there. We really need your support over the coming months.</p>

                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody></table>
                                </td>
                            </tr>
                            <!-- end title bar -->
                            <!-- 1/2 panel [right text] -->
                            <tr style="margin-top: 20px; background: none repeat scroll 0% 0% rgb(247, 246, 244);"><td>
                                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="table600">
                                        <tbody><tr>
                                                <td height="10"></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle" height="30" style="font-size: 12px; color: rgb(139, 136, 131); font-family: arial; line-height: 15px; padding-bottom: 20px; text-align: center; padding-top: 0px;">
                                                    Copyright &copy;
                                                    <a href="http://slapdab.com" style="color:#1BBC9B !important">slapdab.com</a>
                                                    , All rights reserved
                                                    <br><span style="width: 471px; color: rgb(102, 102, 102); font-family: arial; font-size: 10px;">If you do not
                                                            want to receive any more newsletters please follow <a style="color:#1abc9c" href="http://slapdab.com/unsubscribe.php?key=' . base64_encode($email) . '" target="_blank">this
                                                                link</a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                        </tbody></table>';
    $b .='
                                </td></tr>

                        </tbody></table>

                </body></html>';

    /* //mail body */


    /* // Sent eamil here */



    /* Check this email exist or not  */
    $checkSql = "SELECT COUNT(`subscribe_email`) AS total_email FROM `subscribe` WHERE subscribe_email='" . mysqli_real_escape_string($con, $email) . "'";
    $checkSqlResult = mysqli_query($con, $checkSql);
    if ($checkSqlResult) {
        $checkSqlResultRowObj = mysqli_fetch_object($checkSqlResult);
        if ($checkSqlResultRowObj->total_email < 1) {
            $ins = "INSERT INTO `subscribe`(`subscribe_email`, `subscribe_email_sent`) VALUES ('" . mysqli_real_escape_string($con, $email) . "',$email_sent)";
            $insResult = mysqli_query($con, $ins);

            if ($insResult) {
                $return['message'] = 1;
                $email_status = sendEmailFunction($email, $email, 'info@slapdab.com', $Subject, $b);
                if ($email_status == 'true') {
                    $email_sent = 1;
                }


                if ($email_sent == 0) {
                    $return['error'] = 4; //'email seding fail';
                }
            } else {
                $return['error'] = 2; //'insSqlResult query fail';
            }
        } else {
            $return['error'] = 3; //'email exist';
        }
    } else {
        $return['error'] = 1; //'checkSqlResult query fail';
    }
}
echo json_encode($return);
?>
