/**
 * Created by mtanim on 8/13/2015.
 */
!function (a, b, c, d, e) {
    "use strict";
    a.fn.cineSlider = function (f) {
        function g(a) {
            window.console && window.console.log && n.debug && window.console.log(m + ": " + a)
        }

        function h(a) {
            throw m + ": " + a
        }

        function i() {
            for (var b = 0; b < n.shifters.length; b++) {
                var c = a('<div class="cineslider-shifter">');
                c.css("backgroundColor", n.shifters[b]), p.append(c)
            }
            r = p.find(".cineslider-shifter"), g("Shifters created")
        }

        function j() {
            g("Mousewheel event registration"), o.on("mousewheel", b(150, function (a) {
                a.preventDefault(), t || (a.deltaY < 0 ? x() : y())
            }))
        }

        function k() {
            g("TouchSwipe event registration using Hammer.js");
            var a = new e(p[0]);
            a.get("swipe").set({
                direction: e.DIRECTION_VERTICAL,
                velocity: n.swipeVelocity
            }), a.on("swipe", function (a) {
                a.preventDefault(), t || (a.direction === e.DIRECTION_DOWN ? (y(), g("swipedown event triggered")) : a.direction === e.DIRECTION_UP && (x(), g("swipeup event triggered")))
            })
        }

        function l() {
            if (g("Cineslider Plugin launched"), u >= 0 && u < q.length) {
                var a = q.eq(u);
                a.addClass("cineslider-slide-active").siblings(".cineslider-slide").hide(), i(), c.touch ? k() : j(), g("Cineslider Plugin initialized")
            } else h("currentSlide index is not in range")
        }

        var m = "jquery.cineSlider", n = a.extend({
            debug: !1,
            shifters: ["#fff", "#000"],
            duration: 2,
            easing: Power2,
            currentSlide: 1,
            swipeVelocity: .25,
            lock: !1,
            onSlideChangeStart: function (a, b) {
            },
            onSlideChangeComplete: function (a, b) {
            }
        }, f), o = this, p = o.find(".cineslider-wrapper"), q = o.find(".cineslider-slide"), r = null, s = !1, t = n.lock, u = n.currentSlide - 1, v = function () {
            return n.duration + n.duration / 2
        }, w = function (a, b) {
            if (!t && !s && a !== u && a >= 0 && a < q.length) {
                var c = q.eq(u), e = q.eq(a), f = n.duration, h = null, i = new TimelineMax;
                b = void 0 !== b ? b : !1, b && (f = .1), h = a > u ? "down" : "up", d.set(c, {zIndex: 0}), d.set([r, e], {
                    display: "block",
                    zIndex: 1,
                    y: "down" === h ? "100%" : "-100%"
                }), i.add([d.to("down" === h ? r.first() : r.last(), f, {
                    y: "down" === h ? "-100%" : "100%",
                    ease: n.easing.easeIn
                }), d.to("down" === h ? r.last() : r.first(), f, {
                    y: "down" === h ? "-100%" : "100%",
                    ease: n.easing.easeInOut,
                    delay: f / 2
                }), d.to(e, f, {
                    y: "0%",
                    ease: n.easing.easeInOut,
                    delay: f / 2
                })]), i.eventCallback("onStart", function () {
                    s = !0, n.onSlideChangeStart.call(this, c, e, h), o.trigger("slidechangestart", [c, e, h]), g("Slide animation start for slide " + (a + 1))
                }), i.eventCallback("onComplete", function () {
                    d.set([r, c], {display: "none"}), c.removeClass("cineslider-slide-active"), e.addClass("cineslider-slide-active"), u = a, s = !1, n.onSlideChangeComplete.call(this, c, e, h), o.trigger("slidechangecomplete", [c, e, h]), g("Slide animation complete slide " + (a + 1))
                })
            }
        }, x = function () {
            w(u + 1)
        }, y = function () {
            w(u - 1)
        }, z = function () {
            t = !0
        }, A = function () {
            t = !1
        }, B = function () {
            return t
        }, C = function () {
            return s
        }, D = function (a, b) {
            o.on(a, b)
        }, E = function (a, b) {
            o.one(a, b)
        }, F = function (a) {
            o.off(a)
        };
        return l(), {
            target: this,
            slideTo: w,
            slideNext: x,
            slidePrev: y,
            getSlidingDuration: v,
            lock: z,
            unlock: A,
            isLock: B,
            isAnimate: C,
            on: D,
            one: E,
            off: F
        }
    }
}($, $.throttle, Modernizr, TweenMax, Hammer), function (a, b, c, d, e) {
    "use strict";
    d.canvas && (a.fn.frameByFrame = function (c) {
        function f(a) {
            window.console && window.console.log && q.debug && window.console.log(a)
        }

        function g(a, b) {
            b = b || "low";
            var c = q.framePath[b].path, d = q.framePath[b].prefix, e = q.frameExtension;
            return c + d + a + "." + e
        }

        function h(a) {
            var b = t.width / t.height, c = a.width / a.height, d = a.width, e = a.height;
            return b > c ? (d = t.width, e = Math.round(t.width / c)) : (d = Math.round(t.height * c), e = t.height), {
                width: d,
                height: e
            }
        }

        function i() {
            t.width = s.width(), t.height = s.height(), f("Set canvas size width: " + t.width + " height: " + t.height), v.length > 0 && A(w)
        }

        function j(a) {
            var b = h(a), c = 0, d = 0;
            b.width > t.width && (d = -1 * ((b.width - t.width) / 2)), b.height > t.height && (c = -1 * ((b.height - t.height) / 2)), u.drawImage(a, d, c, b.width, b.height)
        }

        function k(b, c, d) {
            var e = new Image;
            void 0 !== c && a(e).on("load", function () {
                c(e)
            }), e.src = b, d && v.push(e)
        }

        function l() {
            var a = 0, b = function (b) {
                var c = Math.round(a / q.frameNumber * 100);
                a++, f("Frame loading progress: " + c + "%"), q.onFramesLoadProgress.call(this, c), s.trigger("framesloadprogress", [c]), a === q.frameNumber && (f("Frames load compeleted"), o(), q.onFramesLoaded.call(), s.trigger("framesloaded"))
            };
            f("Frames loading started");
            for (var c = 0; c < q.frameNumber; c++)k(g(c + 1, "low"), b, !0)
        }

        function m() {
            f("Mousewheel event registration"), s.on("mousewheel", b(q.mousewheelThrottle, function (a) {
                a.preventDefault(), x || (a.deltaY < 0 ? B() : C())
            }))
        }

        function n() {
            f("Touchmove event registration using jquery.kinetic"), s.kinetic({
                x: !1,
                maxvelocity: 8,
                moved: function (a) {
                    x || s.hasClass("kinetic-active") && (s.hasClass("kinetic-moving-down") || s.hasClass("kinetic-decelerating-down") ? B() : (s.hasClass("kinetic-moving-up") || s.hasClass("kinetic-decelerating-up")) && C())
                }
            })
        }

        function o() {
            A(w), d.touch ? n() : m(), q.responsive && r.on("resize", a.debounce(150, i))
        }

        function p() {
            s.append(t), i(), q.loadFramesAfterWindowLoad ? e.ios() ? l() : r.on("load", function () {
                l()
            }) : l()
        }

        var q = a.extend({
            debug: !1,
            responsive: !1,
            mousewheelThrottle: 10,
            loadFramesAfterWindowLoad: !1,
            frameExtension: "jpg",
            frameHighDelay: 1,
            frameNumber: 0,
            framePath: {high: {path: "", prefix: ""}, low: {path: "", prefix: ""}},
            onFramesLoaded: function () {
            },
            onFramesLoadProgress: function (a) {
            },
            onFrameDraw: function (a, b, c) {
            },
            onSequenceStart: function () {
            },
            onSequenceReverse: function () {
            },
            onSequenceComplete: function () {
            }
        }, c), r = a(window), s = a(this), t = document.createElement("canvas"), u = t.getContext("2d"), v = [], w = 0, x = !1, y = null, z = function () {
            i()
        }, A = function (a) {
            if (a >= 0 && a < q.frameNumber) {
                var b = v[a], c = g(a + 1, "high"), d = null, e = null;
                a !== w && (e = a > w ? "down" : "up"), w = a, d = w / q.frameNumber, q.onFrameDraw.call(this, w, e, d), s.trigger("framedraw", [w, e, d]), 1 === w && "down" === e && (f("Frame sequence started"), q.onSequenceStart.call(), s.trigger("sequencestart")), f("Draw low resolution frame (" + (a + 1) + ")"), j(b), clearTimeout(y), y = setTimeout(function () {
                    f("Load High resolution frame (" + (a + 1) + ")"), k(c, function (b) {
                        f("High resolution frame (" + (a + 1) + ") loaded"), j(b)
                    })
                }, 1e3 * q.frameHighDelay)
            } else 0 > a ? (f("Frame sequence reversed"), q.onSequenceReverse.call(), s.trigger("sequencereverse")) : a >= q.frameNumber && (f("Frame sequence completed"), q.onSequenceComplete.call(), s.trigger("sequencecomplete"))
        }, B = function () {
            A(w + 1)
        }, C = function () {
            A(w - 1)
        }, D = function (a, b, c) {
            var d = {checker: w, increaser: w};
            w !== a && TweenMax.to(d, b, {
                increaser: a, onUpdate: function () {
                    var b = Math.ceil(d.increaser);
                    d.checker !== b && (d.checker = b, A(b), void 0 !== c && b === a && c.call())
                }, ease: SteppedEase.config(50)
            })
        }, E = function () {
            x = !0
        }, F = function () {
            x = !1
        }, G = function (a, b) {
            s.on(a, b)
        }, H = function (a, b) {
            s.one(a, b)
        }, I = function (a) {
            s.off(a)
        };
        return p(), {
            target: this,
            setFrame: A,
            setNextFrame: B,
            setPrevFrame: C,
            setFrameSmooth: D,
            refresh: z,
            lock: E,
            unlock: F,
            on: G,
            one: H,
            off: I
        }
    })
}($, $.throttle, $.Kinetic, Modernizr, device), function (a, b, c, d) {
    "use strict";
    a.fn.splitter = function (b) {
        function e() {
            var a = k.first(), b = k.last();
            return a.position().top === b.position().top
        }

        function f(a) {
            if (!l && !m && !a.hasClass("active")) {
                var b = a.siblings(".splitter-item");
                a.addClass("active"), b.removeClass("active"), l = !0, m = !0, e() ? (d.to(b, h.duration, {
                    x: "auto" === b.css("left") ? "50%" : "-50%",
                    ease: h.easing.easeInOut
                }), d.to(a, h.duration, {
                    css: {width: "100%"}, ease: h.easing.easeInOut, onComplete: function () {
                        l = !1
                    }
                })) : (d.to(b, h.duration, {
                    y: "auto" === b.css("bottom") ? "-50%" : "50%",
                    ease: h.easing.easeInOut
                }), d.to(a, h.duration, {
                    css: {height: "100%"}, ease: h.easing.easeInOut, onComplete: function () {
                        l = !1
                    }
                }))
            }
        }

        function g() {
            k.on("click", ".splitter-button", function (b) {
                h.preventClick && b.preventDefault();
                var c = a(this), e = c.attr("href").replace("#", ""), g = a(b.delegateTarget);
                h.hideButtonOnClick && d.to(c, h.duration, {
                    alpha: 0,
                    ease: h.easing.easeInOut
                }), f(g), h.onButtonClick.call(this, e), j.trigger("buttonclick", [e])
            }), i.on("resize", c(150, function () {
                m || (k.removeAttr("style"), e() ? d.set(k, {
                    x: "0%",
                    width: "50%",
                    height: "100%"
                }) : d.set(k, {y: "0%", width: "100%", height: "50%"}))
            }))
        }

        var h = a.extend({
            debug: !1,
            duration: .8,
            easing: Power2,
            hideButtonOnClick: !0,
            preventClick: !0,
            onButtonClick: function (a) {
            }
        }, b), i = a(window), j = a(this), k = j.find(".splitter-item"), l = !1, m = !1, n = function (a) {
            a = void 0 !== a ? a : !0, !l && m && a && (k.removeAttr("style"), e() ? d.set(k, {
                x: "0%",
                width: "50%",
                height: "100%"
            }) : d.set(k, {
                y: "0%",
                height: "50%",
                width: "100%"
            }), k.removeClass("active").find(".splitter-button").removeAttr("style"), m = !1)
        }, o = function () {
            return h.duration
        }, p = function (a, b) {
            j.on(a, b)
        }, q = function (a, b) {
            j.one(a, b)
        }, r = function (a) {
            j.off(a)
        };
        return g(), {target: this, getDuration: o, reset: n, on: p, one: q, off: r}
    }
}($, Modernizr, $.throttle, TweenMax), function (a, b, c, d, e) {
    "use strict";
    a.fn.starwarsScroll = function (c) {
        function f(a) {
            window.console && window.console.log && s.debug && window.console.log(r + ": " + a)
        }

        function g(a) {
            throw r + ": " + a
        }

        function h() {
            x = a("<div>"), y = a("<div>"), x.append(y).appendTo(u).css({
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: "100%",
                overflow: "hidden"
            })
        }

        function i() {
            w.each(function (b) {
                var c = b * s.offsetSentenceDistance, d = 1 - s.offsetScale * b, e = 1 - s.offsetAlpha * b;
                TweenMax.set(a(this), {y: c + "%", scale: d, alpha: e})
            }), f("setup sentences transform before to build the sequence timeline")
        }

        function j() {
            var b = Math.round(1 / s.offsetAlpha);
            w.each(function (c) {
                var d = 0, e = c, f = s.stagger * c;
                for (d = 0; e > d; d++)E.to(a(this), s.duration, {
                    y: s.offsetSentenceDistance * (e - d - 1) + "%",
                    alpha: 1 - s.offsetAlpha * (e - d - 1),
                    scale: 1 - s.offsetScale * (e - d - 1),
                    ease: Linear.easeNone
                }, f), f += s.duration;
                if (c < w.length - 1)for (d = 0; b > d; d++)E.to(a(this), s.duration, {
                    y: -s.offsetSentenceDistance * (d + 1) + "%",
                    alpha: 1 - s.offsetAlpha * (d + 1),
                    scale: 1 + s.offsetScale * (d + 1),
                    ease: Linear.easeNone
                }, f), f += s.duration
            }), f("sentences sequence created")
        }

        function k() {
            var b = 0;
            w.each(function () {
                b += a(this).height() + parseInt(a(this).css("marginBottom").replace("px", ""), 10)
            }), D = b, null !== y && y.height(D + t.height()), f("calculate scrollHeight parameter: " + D)
        }

        function l() {
            t.on("resize", b(150, function () {
                F()
            }))
        }

        function m() {
            var a = [], b = null, c = 30;
            return function (d) {
                if (0 === d)return d;
                if (null !== b)return d * b;
                var e = Math.abs(d);
                a:do {
                    for (var f = 0; f < a.length; ++f)if (e <= a[f]) {
                        a.splice(f, 0, e);
                        break a
                    }
                    a.push(e)
                } while (!1);
                var g = c / a[Math.floor(a.length / 3)];
                return 500 == a.length && (b = g), d * g
            }
        }

        function n() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (a) {
                    window.setTimeout(a, 1e3 / 60)
                }
        }

        function o() {
            f("Mousewheel event registration");
            var a, b = e.parse(navigator.userAgent), c = 0, d = 0, g = 0, h = .9, i = 0, j = "Firefox" === b.browser.family ? 2 : 4, k = .1, l = n();
            m(), "Mac OS X" === b.os.family && (j = "Firefox" === b.browser.family ? .2 : .8);
            var o = function (a) {
                d += a, i += (d - g) * j, g = d
            }, p = function () {
                (-k > i || i > k) && (c += i, B >= 0 && D >= B ? B = -c : 0 > B ? (B = 0, i = 0) : (B = D, i = 0), i *= h, C = B / D, C >= 1 ? C = 1 : 0 >= C && (C = 0), 1 === E.progress() && 1 === C && (s.onComplete.call(), u.trigger("complete")), E.progress(C))
            }, q = function () {
                l(q), A || p()
            };
            u.on("mousewheel", function (b) {
                if (b.preventDefault(), !A) {
                    var d = b.originalEvent, e = d.detail ? -1 * d.detail : d.wheelDelta / 40, f = 0;
                    e || (e = b.deltaY), f = 0 > e ? -1 : 1, f !== a && (i = 0, a = f), c = -1 * B, o(e)
                }
            }), q()
        }

        function p() {
            f("Touchmove event registration using jquery.kinetic"), z = x.kinetic({
                x: !1, moved: function (a) {
                    A || (B = a.scrollTop, C = B / D, C > 1 ? C = 1 : 0 > C && (C = 0), E.progress(C))
                }
            })
        }

        function q() {
            s.offsetAlpha <= 0 && g("offsetAlpha parameter must be greater then zero"), d.touch && h(), k(), i(), j(), d.touch ? p() : o(), l(), s.onInit.call(), f("plugin initialized")
        }

        var r = "jquery.starwars", s = a.extend({
            debug: !1,
            offsetSentenceDistance: 100,
            offsetScale: .25,
            offsetAlpha: .25,
            stagger: .5,
            duration: 1,
            lock: !1,
            onInit: function () {
            },
            onProgress: function (a) {
            },
            onComplete: function () {
            },
            onStart: function () {
            },
            onReverse: function () {
            }
        }, c), t = a(window), u = a(this), v = u.find(".starwars-wrapper"), w = v.find("p"), x = null, y = null, z = null, A = s.lock, B = 0, C = 0, D = 0, E = new TimelineMax({
            paused: !0,
            ease: Linear.easeNone,
            onStart: function () {
                s.onStart.call(), u.trigger("start")
            },
            onComplete: function () {
                s.onComplete.call(), u.trigger("complete")
            },
            onReverseComplete: function () {
                s.onReverse.call(), u.trigger("reverse")
            },
            onUpdate: function () {
                s.onProgress.call(this, E.progress()), u.trigger("progress", [E.progress()])
            }
        }), F = function () {
            var a = D, b = B;
            k(), a != D && (f("scrollTop recalc because scrollHeight is changed"), B = b * D / a, null !== x && x.kinetic("scrollTop", B))
        }, G = function () {
            B = 0, C = 0, E.progress(0)
        }, H = function () {
            A = !0
        }, I = function () {
            A = !1
        }, J = function (a, b) {
            u.on(a, b)
        }, K = function (a, b) {
            u.one(a, b)
        }, L = function (a) {
            u.off(a)
        };
        return q(), {target: this, lock: H, unlock: I, reset: G, refresh: F, on: J, one: K, off: L}
    }
}($, $.throttle, $.Kinetic, Modernizr, detect), function (a, b, c, d) {
    "use strict";
    a.fn.videoBg = function (e) {
        function f(a) {
            window.console && window.console.log && n.debug && window.console.log(m + ": " + a)
        }

        function g(a) {
            throw m + ": " + a
        }

        function h() {
            var b = n.viewport.width() / n.viewport.height(), c = q.videoWidth / q.videoHeight, d = q.videoWidth, e = q.videoHeight, g = 0, h = 0;
            b > c ? (d = n.viewport.width(), e = Math.round(n.viewport.width() / c)) : (d = Math.round(n.viewport.height() * c), e = n.viewport.height()), d > n.viewport.width() && (g = -((d - n.viewport.width()) / 2)), e > n.viewport.height() && (h = -((e - n.viewport.height()) / 2)), a(q).css({
                width: d,
                height: e,
                marginLeft: g,
                marginTop: h
            }), f("Video resized")
        }

        function i() {
            var c = a("<video>");
            c.addClass("videojs vjs-default-skin").appendTo(p), f("Tag Video created and appended into main container"), r = d(c[0]), r.on("loadedmetadata", function (a) {
                q = a.target, h(), o.on("resize", b(150, h)), n.onVideoLoad.call(), p.trigger("videoload"), f("onloadedmetadata callback launched")
            }), f("Register videojs loadeddata callback"), r.one("play", function () {
                n.onVideoFirstPlay.call(), p.trigger("videofirstplay"), f("onPlay callback launched")
            }), f("Register videojs play event callback"), r.preload("auto").loop(n.loop).autoplay(n.autoplay).muted(n.muted).src(n.source), f("Player and Video created"), k()
        }

        function j() {
            var b = new Image;
            a(b).on("load", function () {
                p.css("backgroundImage", "url(" + n.poster + ")"), f("Poster created. Maybe you are using a mobile device or an old browser"), n.onPosterLoaded.call(), p.trigger("posterloaded"), f("onPosterLoaded callback launched")
            }), f("Register posterload callback"), k(), b.src = n.poster
        }

        function k() {
            if (n.shadow && n.shadowAlpha > 0 && n.shadowAlpha < 1 && "transparent" !== n.shadowColor) {
                var b = a("<div>");
                b.addClass("videobg-shadow").css({
                    opacity: n.shadowAlpha,
                    backgroundColor: n.shadowColor
                }).appendTo(p), f("Shadow created (color: " + n.shadowColor + ", alpha: " + n.shadowAlpha + ")")
            }
        }

        function l() {
            f("Plugin launched"), c.touch || void 0 === n.source ? void 0 !== n.poster ? j() : g('Needs to define "source" or "poster" parameter') : i()
        }

        var m = "jquery.videoBg", n = a.extend({
            debug: !1,
            source: void 0,
            muted: !0,
            autoplay: !0,
            loop: !0,
            poster: void 0,
            shadow: !0,
            shadowAlpha: .3,
            shadowColor: "#000",
            viewport: a(window),
            onVideoFirstPlay: function () {
            },
            onPosterLoaded: function () {
            },
            onVideoLoad: function () {
            }
        }, e), o = a(window), p = a(this), q = null, r = null, s = function () {
            return r
        }, t = function () {
            r && r.play()
        }, u = function () {
            r && r.pause()
        }, v = function () {
            r && r.pause().currentTime(0)
        }, w = function (a, b) {
            p.on(a, b)
        }, x = function (a, b) {
            p.one(a, b)
        }, y = function (a) {
            p.off(a)
        };
        return l(), {target: this, getPlayer: s, play: t, pause: u, stop: v, on: w, one: x, off: y}
    }
}($, $.throttle, Modernizr, videojs), function (a, b, c, d) {
    "use strict";
    a.fn.videoPopup = function (e) {
        function f(a) {
            window.console && window.console.log && h.debug && window.console.log(g + ": " + a)
        }

        var g = "jquery.videoPopup", h = a.extend({
            debug: !1, duration: .8, easing: Power2, onBeforeShow: function () {
            }, onAfterShow: function () {
            }, onBeforeHide: function () {
            }, onAfterHide: function () {
            }
        }, e);
        return this.each(function () {
            function e() {
                v && (v.play(), f("play video"))
            }

            function g() {
                v && (v.pause(), f("pause video"))
            }

            function i() {
                v && (v.pause(), v.currentTime(0), f("stop video"))
            }

            function j() {
                c.fromTo(r, h.duration / 2, {css: {display: "block", alpha: 0}}, {
                    alpha: 1,
                    ease: h.easing.easeOut
                }), f("show close button")
            }

            function k() {
                c.to(r, h.duration / 2, {
                    alpha: 0, ease: h.easing.easeInOut, onComplete: function () {
                        c.set(r, {css: {display: "none"}})
                    }
                }), f("hide close button")
            }

            function l() {
                d(n.find("video")[0]).ready(function () {
                    v = this, s = n.find(".video-js"), q.on("click", function (a) {
                        a.preventDefault(), f("play button clicked"), y()
                    }), b.touch && q.one("click", function (a) {
                        a.preventDefault(), e(), f("force play video on first click (only for mobile)")
                    }), r.on("click", function (a) {
                        a.preventDefault(), a.stopPropagation(), f("close button clicked"), x()
                    }), f("plugin initialized")
                })
            }

            var m = a(window), n = a(this), o = n.find(".video-popup-wrapper"), p = n.find(".video-popup-overlay"), q = n.find(".video-popup-play"), r = n.find(".video-popup-close"), s = null, t = n.outerWidth(), u = n.outerHeight(), v = null, w = !1, x = function () {
                w || (w = !0, h.onBeforeHide.call(), f("onBeforeHide callback launched"), g(), k(), c.to(s, h.duration / 2, {
                    alpha: 0,
                    onComplete: function () {
                        c.set(s, {
                            css: {
                                display: b.touch ? "block" : "none",
                                visibility: b.touch ? "hidden" : "visible"
                            }
                        })
                    }
                }), c.fromTo(n, h.duration, {
                    css: {
                        top: "50%",
                        left: "50%",
                        marginLeft: -m.width() / 2,
                        marginTop: -m.height() / 2
                    }
                }, {
                    css: {width: t, height: u, marginLeft: -t / 2, marginTop: -u / 2},
                    ease: h.easing.easeInOut,
                    onComplete: function () {
                        n.removeAttr("style"), c.to(o, h.duration / 2, {
                            alpha: 1,
                            ease: h.easing.easeInOut
                        }), f("show video-popup wrapper")
                    }
                }), f("reduce video-popup container to original size"), c.to(p, h.duration, {
                    alpha: 0,
                    ease: Power2.easeInOut,
                    onComplete: function () {
                        c.set(p, {
                            css: {
                                display: b.touch ? "block" : "none",
                                visibility: b.touch ? "hidden" : "visible"
                            }
                        }), i(), h.onAfterHide.call(), w = !1, f("onAfterHide callback launched")
                    }
                }), f("hide video-popup overlay"))
            }, y = function () {
                w || (w = !0, t = n.outerWidth(), u = n.outerHeight(), h.onBeforeShow.call(), f("onBeforeShow callback launched"), c.to(o, h.duration / 2, {alpha: 0}), f("hide video-popup wrapper"), c.fromTo(n, h.duration, {
                    position: "fixed",
                    zIndex: 9999999
                }, {
                    css: {width: "100%", height: "100%", marginLeft: -m.width() / 2, marginTop: -m.height() / 2},
                    ease: h.easing.easeInOut,
                    onComplete: function () {
                        c.set(n, {css: {top: 0, left: 0, marginLeft: 0, marginTop: 0}})
                    }
                }), f("set window size to video-popup container"), c.fromTo(p, h.duration, {
                    css: {
                        display: "block",
                        visibility: "visible",
                        alpha: 0
                    }
                }, {
                    alpha: 1, ease: h.easeInOut, onComplete: function () {
                        e(), j(), w = !1, c.fromTo(s, h.duration / 2, {
                            css: {
                                display: "block",
                                visibility: "visible",
                                alpha: 0
                            }
                        }, {alpha: 1}), h.onAfterShow.call(), f("onAfterShow callback launched")
                    }
                }), f("show video-popup overlay"))
            };
            return l(), {target: this}
        })
    }
}($, Modernizr, TweenMax, videojs), function (a) {
    "use strict";
    function b(a) {
        this.el = a, this.overlay = this.el.querySelector(".nl-overlay"), this.fields = [], this.fldOpen = -1, this._init()
    }

    function c(a, b, c, d) {
        this.form = a, this.elOriginal = b, this.pos = d, this.type = c, this._create(), this._initEvents()
    }

    var d = a.document;
    String.prototype.trim || (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    }), b.prototype = {
        _init: function () {
            var a = this;
            Array.prototype.slice.call(this.el.querySelectorAll("select")).forEach(function (b, d) {
                a.fldOpen++, a.fields.push(new c(a, b, "dropdown", a.fldOpen))
            }), Array.prototype.slice.call(this.el.querySelectorAll("input")).forEach(function (b, d) {
                a.fldOpen++, a.fields.push(new c(a, b, "input", a.fldOpen))
            }), this.overlay.addEventListener("click", function (b) {
                a._closeFlds()
            }), this.overlay.addEventListener("touchstart", function (b) {
                a._closeFlds()
            })
        }, _closeFlds: function () {
            -1 !== this.fldOpen && this.fields[this.fldOpen].close()
        }
    }, c.prototype = {
        _create: function () {
            "dropdown" === this.type ? this._createDropDown() : "input" === this.type && this._createInput()
        }, _createDropDown: function () {
            var a = this;
            this.fld = d.createElement("div"), this.fld.className = "nl-field nl-dd", this.toggle = d.createElement("a"), this.toggle.innerHTML = this.elOriginal.options[this.elOriginal.selectedIndex].innerHTML, this.toggle.className = "nl-field-toggle", this.optionsList = d.createElement("ul");
            var b = "";
            Array.prototype.slice.call(this.elOriginal.querySelectorAll("option")).forEach(function (c, d) {
                b += a.elOriginal.selectedIndex === d ? '<li class="nl-dd-checked">' + c.innerHTML + "</li>" : "<li>" + c.innerHTML + "</li>", a.elOriginal.selectedIndex === d && (a.selectedIdx = d)
            }), this.optionsList.innerHTML = b, this.fld.appendChild(this.toggle), this.fld.appendChild(this.optionsList), this.elOriginal.parentNode.insertBefore(this.fld, this.elOriginal), this.elOriginal.style.display = "none"
        }, _createInput: function () {
            this.fld = d.createElement("div"), this.fld.className = "nl-field nl-ti-text", this.toggle = d.createElement("a"), this.toggle.innerHTML = this.elOriginal.getAttribute("placeholder"), this.toggle.className = "nl-field-toggle", this.optionsList = d.createElement("ul"), this.getinput = d.createElement("input"), this.getinput.setAttribute("type", "text"), this.getinput.setAttribute("placeholder", this.elOriginal.getAttribute("placeholder")), this.getinputWrapper = d.createElement("li"), this.getinputWrapper.className = "nl-ti-input", this.inputsubmit = d.createElement("button"), this.inputsubmit.className = "nl-field-go", this.inputsubmit.innerHTML = "Go", this.getinputWrapper.appendChild(this.getinput), this.getinputWrapper.appendChild(this.inputsubmit), this.example = d.createElement("li"), this.example.className = "nl-ti-example", this.example.innerHTML = this.elOriginal.getAttribute("data-subline"), this.optionsList.appendChild(this.getinputWrapper), this.optionsList.appendChild(this.example), this.fld.appendChild(this.toggle), this.fld.appendChild(this.optionsList), this.elOriginal.parentNode.insertBefore(this.fld, this.elOriginal), this.elOriginal.style.display = "none"
        }, _initEvents: function () {
            var a = this;
            if (this.toggle.addEventListener("click", function (b) {
                    b.preventDefault(), b.stopPropagation(), a._open()
                }), this.toggle.addEventListener("touchstart", function (b) {
                    b.preventDefault(), b.stopPropagation(), a._open()
                }), "dropdown" === this.type) {
                var b = Array.prototype.slice.call(this.optionsList.querySelectorAll("li"));
                b.forEach(function (c, d) {
                    c.addEventListener("click", function (d) {
                        d.preventDefault(), a.close(c, b.indexOf(c))
                    })
                })
            } else"input" === this.type && (this.getinput.addEventListener("keydown", function (b) {
                13 == b.keyCode && a.close()
            }), this.inputsubmit.addEventListener("click", function (b) {
                b.preventDefault(), a.close()
            }), this.inputsubmit.addEventListener("touchstart", function (b) {
                b.preventDefault(), a.close()
            }))
        }, _open: function () {
            if (this.open)return !1;
            this.open = !0, this.form.fldOpen = this.pos;
            this.fld.className += " nl-field-open", void 0 !== this.getinput && this.getinput.focus()
        }, close: function (a, b) {
            if (!this.open)return !1;
            if (this.open = !1, this.form.fldOpen = -1, this.fld.className = this.fld.className.replace(/\b nl-field-open\b/, ""), "dropdown" === this.type) {
                if (a) {
                    var c = this.optionsList.children[this.selectedIdx];
                    c.className = "", a.className = "nl-dd-checked", this.toggle.innerHTML = a.innerHTML, this.selectedIdx = b, this.elOriginal.value = this.elOriginal.children[this.selectedIdx].value
                }
            } else"input" === this.type && (this.getinput.blur(), this.toggle.innerHTML = "" !== this.getinput.value.trim() ? this.getinput.value : this.getinput.getAttribute("placeholder"), this.elOriginal.value = this.getinput.value)
        }
    }, a.NLForm = b
}(window), function (a, b, c) {
    "use strict";
    a.fn.nlform = function (b) {
        function d(a) {
            window.console && window.console.log && l.debug && window.console.log(k + ": " + a)
        }

        function e() {
            function b(a) {
                return 0 === a.val().length
            }

            function c(a) {
                return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a.val())
            }

            var d = [];
            return n.each(function () {
                a(this).is('input[type="text"]') || a(this).is("textarea") || a(this).is("select") ? b(a(this)) && d.push(a(this)) : a(this).is('input[type="email"]') && (c(a(this)) || d.push(a(this)))
            }), a(d)
        }

        function f() {
            p.stop().fadeOut(), d("hide messages")
        }

        function g(a) {
            f(), p.filter("." + a).stop().fadeIn(), d("show " + a + " message")
        }

        function h() {
            m.find(".nl-field").removeClass("nl-field-error")
        }

        function i() {
            o.text(o.data("text")).removeAttr("disabled").show()
        }

        function j() {
            new c(m[0]), n = m.find("input[name], select[name], textarea[name]"), o = m.find('button[type="submit"]'), p = m.find("p.error, p.success"), o.data("text", o.text()), m.on("submit", function (b) {
                b.preventDefault();
                var c = e();
                c.length > 0 ? (h(), c.each(function () {
                    a(this).prev(".nl-field").addClass("nl-field-error")
                }), g("error")) : (h(), f(), o.text(o.data("send-text")).addClass("send").attr("disabled", "disabled"), a.ajax({
                    url: m.attr("action"),
                    method: "POST",
                    data: m.serialize(),
                    success: function (a) {
                        a === l.successCheck ? (g("success"), o.hide()) : (g("error"), i())
                    }
                }))
            }), d("initialize")
        }

        var k = "jquery.nlform", l = a.extend({
            debug: !1,
            successCheck: "1"
        }, b), m = a(this), n = null, o = null, p = null, q = function () {
            f(), h(), i()
        };
        return j(), {target: this, reset: q}
    }
}($, Modernizr, NLForm);