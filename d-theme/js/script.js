
$(window).on('load', function () {

    $("#preLoader").addClass("fadeOut").delay(1000).queue(function(next){
        $(this).addClass("hide");
        next();
    });
});



$(document).ready(function() {


    if ($(window).width() > 768) {
        //  $('.fullpageSelector').fullpage();

        $('#fullpage').fullpage({
            anchors: ['firstPage', 'secondPage', '3rdPage', '4thPage', '5thPage','lastPage'],
            menu: '#menu',
            scrollBar: false,

            'verticalCentered': false,
            autoScrolling: true,

            'css3': true,
            'navigation': true,
            'navigationPosition': 'right',

            'afterLoad': function(anchorLink, index){
                if(index == 2){
                    $('#iphone3, #iphone2, #iphone4').addClass('active');
                    $('header').removeClass('activex');
                }


                if(index == 1){
                    $('header').addClass('activex');
                }
            },

            'onLeave': function(index, nextIndex, direction){
                if (index == 1 && direction == 'down'){
                    $('header').eq(index -1).removeClass('moveDown').addClass('moveUp');
                }

                if (index == 3 && direction == 'down'){
                    $('.section').eq(index -1).removeClass('moveDown').addClass('moveUp');
                }
                else if(index == 3 && direction == 'up'){
                    $('.section').eq(index -1).removeClass('moveUp').addClass('moveDown');
                }

                $('#staticImg').toggleClass('active', (index == 2 && direction == 'down' ) || (index == 4 && direction == 'up'));
                $('#staticImg').toggleClass('moveDown', nextIndex == 4);
                $('#staticImg').toggleClass('moveUp', index == 4 && direction == 'up');
            }
        });

    } else {


            $('#fullpage').fullpage({
                verticalCentered: true,
                navigation: false,
                autoScrolling: false,
				fitToSection: false,
                slidesNavigation: true,
                slidesNavPosition: 'bottom',
                loopHorizontal: false,
                afterRender: function(){
                    $('.section').css('height', 'auto');
                },
                afterResize: function(){
                    $('.section').css('height', 'auto');
                },
            });
    }



});


