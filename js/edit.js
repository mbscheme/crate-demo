$(document).ready(function() {
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'popup';




    //make status editable
    $('#slide-title').editable({
        type: 'textarea',
        title: 'Select status',
        placement: 'bottom',
        value: 'Slide Title'

        /* source: [
         {value: 1, text: 'status 1'},
         {value: 2, text: 'status 2'},
         {value: 3, text: 'status 3'}
         ]

         //uncomment these lines to send data on server
         ,pk: 1
         ,url: '/post'
         */
    });

    //make status editable
    $('#slide-description').editable({
        type: 'textarea',
        title: 'Select status',
        placement: 'bottom',
        value: 'Slide Description'

        /* source: [
            {value: 1, text: 'status 1'},
            {value: 2, text: 'status 2'},
            {value: 3, text: 'status 3'}
        ]

         //uncomment these lines to send data on server
         ,pk: 1
         ,url: '/post'
         */
    });
});