
$(document).ready(function() {
    function bindNavbar() {
        if ($(window).width() > 768) {
            $('.navbar-site .dropdown').on('mouseover', function(){
                $('.dropdown-toggle', this).next('.dropdown-menu').show();
            }).on('mouseout', function(){
                $('.dropdown-toggle', this).next('.dropdown-menu').hide();
            });

            $('.dropdown-toggle').click(function() {
                if ($(this).next('.dropdown-menu').is(':visible')) {
                    window.location = $(this).attr('href');
                }
            });
        }
        else {
            $('.navbar-site .dropdown').off('mouseover').off('mouseout');
        }
    }

    $(window).resize(function() {
        bindNavbar();
    });

    bindNavbar();
});



$(document).ready(function() {

   // var offset = $('.main-nav').offset();
    var logoimg2 = 'images/tradehaven-c.png';
    var logoimg = 'images/tradehaven.png';

    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        // check the visible top of the browser

        if (250 < scrollTop) {
            $(".header").addClass('is-sticky');
            $(".navbar-nav").addClass('white');
            $(".navbar-nav").addClass('navbar-white');

            //$('.logo img').addClass('c').attr("src").replace(".png", "-c.png");
            $(".logo img").attr('src',logoimg2);

            } else {
             $(".header").removeClass('is-sticky');
            $(".navbar-nav").removeClass('navbar-white');
             $(".navbar-nav").removeClass('white');
             $(".logo img").attr('src',logoimg);

        }


    });
});


$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        $("#slide-promox").addClass('visible');
    }else {
        $("#slide-promox").removeClass('visible');
    }
});


$('.navbar-site .dropdownx').each(function () {
    var delay;
    var delayNav = $(this);
    var menu = delayNav.find('.dropdown-menu');


    delayNav.hover(function() {
    // Mouse over
        delay = setTimeout(function () {
            delayNav.addClass('open');
           // menu.css({"display":"block"});
        }, 500);
    }, function() {
    // Mouse leave
        clearTimeout(delay);
        delayNav.removeClass('open');
    // menu.css({"display":"none"});
    });
});


$(document).on('click', '.navbar-site .dropdown-menu', function(e) {
    e.stopPropagation()
})


