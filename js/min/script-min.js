// Subscribe Modal

$(window).load(function(){
    // full load
    $('#crateModal').modal('show');
    $('#crateModal').removeClass('hide');
});


$(function(){
    $('#successModal').on('show.bs.modal', function(){
        var myModal = $(this);
        clearTimeout(myModal.data('hideInterval'));
        myModal.data('hideInterval', setTimeout(function(){
            myModal.modal('hide');
        }, 1800));
    });
});

// Main Menu Toggle

$(document).ready(function () {
    $(".menu-wrapper").hide();
    $('a.prof').click(function() {
        $(".menu-wrapper").slideToggle();
    });      

    $('.close-icon').click(function() {
        $(".menu-wrapper").slideToggle();
    });          
});

$(document).ready(function () {
    $(".menu-wrapper2").hide();
    $('a.prof').click(function() {
        $(".menu-wrapper2").slideToggle();
    });      

    $('.close-icon').click(function() {
        $(".menu-wrapper2").slideToggle();
    });          
});


// On Click Change Slide Background

$(function(){

    $('.slide-item').click(function(){
        $('.slide-item').removeClass('active');
        $(this).addClass('active');
        var imgbg = $(this).find('img').attr("src");
        //console.log(imgbg);
        $('.panel-add-denote.has-img').addClass('img-show').css({backgroundImage: "url("+imgbg+")"});
    });

});



