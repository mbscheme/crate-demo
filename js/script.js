$(document).ready(function() {
    $("a#slide-nav-hook").click(function(t) {
        return t.stopPropagation(), $("#slide-nav, #wrapper").addClass("open"), $("#slide-nav.right").removeClass("open"), !1
    }), $("#slide-nav").on("click", function(t) {
        $(t.target).hasClass("sign-out") || t.stopPropagation()
    }), $(document).click(function() {
        $("#slide-nav, #wrapper").removeClass("open")
    }), $("a#slide-nav-close").click(function(t) {
        return t.preventDefault(), $("#slide-nav, #wrapper").removeClass("open"), !1
    })
})

// Subscribe Modal

$('.form-s-1-toggle').click(function() {
    $('.form-s-2').removeClass('active');
    $('.form-s-0').removeClass('active');
    $('.form-s-1').addClass('active');
});


$('.form-s-2-toggle').click(function() {
    $('.form-s-1').removeClass('active');
    $('.form-s-0').removeClass('active');
    $('.form-s-2').addClass('active');
});

$('.has-user-modal').click(function() {
    $('.form-s-2').removeClass('active');
    $('.form-s-1').removeClass('active');
    $('.form-s-0').addClass('active');
});


$('.s-bar a').click(function() {
    $('.s-bar a').removeClass('active');
    $(this).addClass('active');
});




$(window).load(function(){
    // full load
    $('#crateModalx').modal('show');
    $('#crateModalx').removeClass('hide');
});


$(function(){
    $('#successModal').on('show.bs.modal', function(){
        var myModal = $(this);
        clearTimeout(myModal.data('hideInterval'));
        myModal.data('hideInterval', setTimeout(function(){
            myModal.modal('hide');
        }, 1800));
    });
});

// Main Menu Toggle

$('.keep-open .dropdown-menu').click(function(e) {
   // e.stopPropagation();
});

$('#slide-nav').click(function(e) {
     e.stopPropagation();
});



$('.dropdown.keep-open').on({
    "shown.bs.dropdown": function() { this.closable = false; },
    "click":             function() { this.closable = true; },
    "hide.bs.dropdown":  function() { return this.closable; }
});

$('.content-area-meta-nav a').click(function() {
    $('.nav-has-active a').removeClass('active');
    $(this).addClass('active');
});

$(document).ready(function () {
    $('a.prof').click(function() {
         $("#menu-wrapper").slideToggle();
         $("#menu-wrapper2").slideUp();
         $("#menu-wrapper3").slideUp();
    });      

    $('.close-icon').click(function() {
        $("#menu-wrapper").slideUp();
    });

    $('.navigation-toggle').click(function() {
        $(this).toggleClass('active');
    });

    $('a.edit').click(function() {
        $("#menu-wrapper2").slideToggle();
        $("#menu-wrapper").slideUp();
        $("#menu-wrapper3").slideUp();
    });      

    $('.close-icon').click(function() {
        $("#menu-wrapper2").slideUp();

    });

    $('a.explore').click(function() {
        $("#menu-wrapper3").slideToggle();
        $("#menu-wrapper").slideUp();
        $("#menu-wrapper2").slideUp();
    });      

    $('.close-icon').click(function() {
        $("#menu-wrapper3").slideUp();

    });           
});





// On Click Change Slide Background

$(function(){

    $('.slide-item').click(function(){
        $('.slide-item').removeClass('active');
        $(this).addClass('active');
        var imgbg = $(this).find('img').attr("src");
        //console.log(imgbg);
        $('.panel-add-denote.has-img').addClass('img-show').css({backgroundImage: "url("+imgbg+")"});
    });

});



jQuery(document).ready(function(){

	  jQuery('.slide-carousel-1').owlCarousel({
		navigation:true,
		navigationText: ['<i class="glyphicon glyphicon-chevron-left"></i>','<i class="glyphicon glyphicon-chevron-right"></i>'],
		slideSpeed : 300,
		pagination: false,
		autoPlay: false,
		items:2,
		itemsDesktop:[1199,2],
		itemsDesktopSmall:[996,2],
		itemsTablet:[768,2],
		itemsMobile:[580,1]
	  });
	   jQuery('.cartepick-carousel').owlCarousel({
		navigation:true,
		navigationText: ['<i class="glyphicon glyphicon-chevron-left"></i>','<i class="glyphicon glyphicon-chevron-right"></i>'],
		slideSpeed : 300,
		pagination: false,
		autoPlay: false,
		items:4,
		itemsDesktop:[1199,4],
		itemsDesktopSmall:[996,3],
		itemsTablet:[768,2],
		itemsMobile:[480,1]
	  });

});